import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        int[] tab = new int[10];
        for (int i = 0; i < tab.length; i++) {
            tab[i] = i;
        }
        for (int i = 1; i < tab.length; i = i + 2) {
            tab[i] = tab[i] + tab[i - 1];
        }

        for (int i = 0; i < tab.length; i++) {
            if (tab[i] % 2 == 0) {
                tab[i] = tab[i] / 2;
            }
        }

        int sum = 0;
        for (int i = 0; i < tab.length; i++) {
            sum = sum + tab[i];

        }

        System.out.println(sum);


        //bubbleSort

        int[] tab2 = {30, 10, 25, 20, 15, 5};
        for (int i = 0; i < tab2.length - 1; i++) {
            boolean tejSprawy = false;

            if (tab2[i] > tab2[i + 1]) {
                tejSprawy = true;
                int temp = tab2[i];
                tab2[i] = tab2[i + 1];
                tab2[i + 1] = temp;
            }

        }
        for (int k = 0; k < tab2.length; k++) {
            for (int i = 0; i < tab2.length - 1 - k; i++) {
                if (tab2[i] > tab2[i + 1]) {
                    int temp = tab2[i];
                    tab2[i] = tab2[i + 1];
                    tab2[i + 1] = temp;
                }
            }


            System.out.println();
            for (int i = 0; i < tab2.length; i++) {
                System.out.print(tab2[i] + " ");
            }
        }

        //koniec  bubble sort
// pod spodem drugi przyklad bubble sort
        int x []  = {10, 20, 40, 30 ,50};
        boolean swapped = true;
        while (swapped) {
            swapped = false;
            for (int i = 1; i < x.length; i++) {
                int temp = 0;
                if (x[i - 1] > x[i]) {
                    temp = x[i - 1];
                    x[i - 1]=x[i];
                    x[i] = temp;
                    swapped = true;
                }
            }
        }
        System.out.println();
        for (int i = 0; i < x.length; i++) {
            System.out.print(x[i] + " ");
        }
// LISTA

        List<Person> personList = new ArrayList<>();
        Person p1 = new Person("kamil", "nowakowski1", 10);
        Person p2 = new Person("kamil2", "nowakowski2", 10);
        Person p3 = new Person("kamil3", "nowakowski3", 10);
        Person p4 = new Person("kamil4", "nowakowski4", 10);
        Person p5= new Person("kamil5", "nowakowski5", 10);
        Person p6 = new Person("kamil6", "nowakowski6", 10);
        Person p7 = new Person("kamil7", "nowakowski7", 10);
        Person p8 = new Person("kamil8", "nowakowski8", 10);
        Person p9 = new Person("kamil9", "nowakowski9", 10);
        Person p10 = new Person("kamil10", "nowakowski10", 10);

        personList.add(p1);
        personList.add(p2);
        personList.add(p3);
        personList.add(p4);
        personList.add(p5);
        personList.add(p5);
        personList.add(p6);
        personList.add(p7);
        personList.add(p8);
        personList.add(p9);
        personList.add(p10);

        System.out.println("LISTA =========");

        for (Person p : personList) {
            System.out.println(p);
        }
        personList.remove(p1);
        System.out.println("lista po usunieciu =======");
        for (Person p : personList) {
            System.out.println(p);
        }
        System.out.println("pierwszy i ostatni element");

        if(personList.size() > 0) {
            System.out.println(personList.get(0));
            System.out.println(personList.get(personList.size() - 1));
        }
        System.out.println("Czy " + p2 + " jest na liście " + personList.contains(p2));
        System.out.println(personList.containsAll(personList));
        System.out.println("Rozmiar listy " + personList.size());
    }
}
